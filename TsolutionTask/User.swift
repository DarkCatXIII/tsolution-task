//
//  User.swift
//  TsolutionTask
//
//  Created by DarkCatXIII on 9/22/19.
//  Copyright © 2019 AT. All rights reserved.
//

import Foundation


struct User: Decodable {
    var id: Int
    var name: String
    
    init(json: [String:Any]) {
        id = json["id"] as? Int ?? 0
        name = json["name"] as? String ?? ""
    }
}
