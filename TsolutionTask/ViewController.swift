//
//  ViewController.swift
//  TsolutionTask
//
//  Created by DarkCatXIII on 9/22/19.
//  Copyright © 2019 AT. All rights reserved.
//

import UIKit
import Alamofire

struct JSONData: Decodable {
    let data:[UserData]
}


struct UserData: Decodable {
    var id: Int
    var name: String
    
    init(json: [String:Any]) {
        id = json["id"] as? Int ?? 0
        name = json["name"] as? String ?? ""
    }
}




class ViewController: UIViewController {

    @IBOutlet weak var elementTF: UITextField!
    @IBOutlet weak var elementIdLbl: UILabel!
    @IBOutlet weak var dateTF: UITextField!
    
    var thePickerElements = UIPickerView()
    var thePickerDate = UIDatePicker()
    
    var dataFormAPI:[Any] = [Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        elementTF.delegate = self
        dateTF.delegate = self
        thePickerElements.delegate = self
        thePickerElements.dataSource = self
        dateTF.inputView = thePickerDate
        elementTF.isUserInteractionEnabled = false
        
        //MARK: ALAMOFIRE
        getDataAlamoFire()
        
        //MARK: URLSESSION
//        getData()
    }
    
    
    @objc func nextClick() {
        dateTF.becomeFirstResponder();
    }

    @objc func doneClick(){
        dateTF.resignFirstResponder()
    }
    
    @objc func dateChanged(_ sender: UIDatePicker) {
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateStyle = .medium
        dateFormatter1.timeStyle = .none
        dateFormatter1.dateFormat = "yyyy/MM/dd"
        dateTF.text = dateFormatter1.string(from: thePickerDate.date)
    }
    
    
    
    func pickUpOrganize(_ textField : UITextField){
        var doneButton:UIBarButtonItem!
        if(textField.tag == 1){
            thePickerElements = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            thePickerElements.delegate = self
            thePickerElements.dataSource = self
            textField.inputView = thePickerElements
            
            
             doneButton = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(ViewController.nextClick))
        }else{
            thePickerDate = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            thePickerDate.datePickerMode = UIDatePicker.Mode.date
            thePickerDate.maximumDate = Date()
            thePickerDate.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)

            textField.inputView = thePickerDate
            
            doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(ViewController.doneClick))
        }
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    
    //MARK: USE ALAMOFIRE
    func getDataAlamoFire(){
        
        
        guard let url = URL(string:"http://demovisiontsol.com/optibov/api/lists/sucklingSystems") else {return}
        
        let parameters = [
            "lang_id": 1, "apiToken":"avwE9kC13N6FvobrmpLpd7B7K57w6eHjS622GGI16g3KCcBKmMipjZ1Bj0pFjDewiFyRnAWUuli2xGHW"] as [String : Any]
        
        
        Alamofire.request(url, method: .get, parameters: parameters).validate()
            .responseJSON { response in
                
                switch response.result {
                case .success(let value):
                    self.completionHandler(response: value as? [String:Any], error: nil)
                case .failure(let error):
                    self.completionHandler(response: nil, error: error)
                }
                
        }
    }
    
    func completionHandler(response:[String:Any]?,error:Error?) {
        
        if((error) != nil){
            elementTF.isUserInteractionEnabled = false
            let msg = error!.localizedDescription
            
            let alert = UIAlertController(title: "Error", message: msg, preferredStyle: .alert)
                
            alert.addAction(UIAlertAction(title: "Later", style: .cancel,handler:nil))
                
            alert.addAction(UIAlertAction(title: "Retry", style: .default,handler: { action in
                self.getDataAlamoFire()
            }))
            self.present(alert, animated: true)
                    
        }else{
            
            let data: [Any] = response!["data"] as! [Any]
            
            data.forEach(){ element in
                let user = UserData(json: element as! [String:Any])
                dataFormAPI.append(user)
            }
            print(data)
            
            elementTF.isUserInteractionEnabled = true
        }
        
        
        
    }
    
    
    //MARK: USE URLSESSION
    func getData(){
        let session = URLSession(configuration: .default)
        guard let url = URL(string: "http://demovisiontsol.com/optibov/api/lists/sucklingSystems?lang_id=1&apiToken=avwE9kC13N6FvobrmpLpd7B7K57w6eHjS622GGI16g3KCcBKmMipjZ1Bj0pFjDewiFyRnAWUuli2xGHW") else { return }
        let request =  URLRequest(url: url)
        
        let task = session.dataTask(with: request, completionHandler: {data, response, error in
            
            guard let data = data else { return }
            
            do{
                let JSonData = try JSONDecoder().decode(JSONData.self, from: data)
                
                self.dataFormAPI = JSonData.data
            }catch let Jerr {
                print(Jerr)
            }
            
        })
        task.resume()
    }

}

extension ViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //MARK: delegate method
        if(textField.tag == 1){
            elementTF.text = (dataFormAPI[0] as! UserData).name
            elementIdLbl.text = " Element ID: \((dataFormAPI[0] as! UserData).id)"
            pickUpOrganize(textField)
        }else{
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "yyyy/MM/dd"
            dateTF.text = dateFormatter1.string(from: Date())
            pickUpOrganize(textField)
        }
    }
    
}


extension ViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataFormAPI.count
    }
    
    
    //MARK: Delegates
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return (dataFormAPI[row] as! UserData).name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        elementTF.text = (dataFormAPI[row] as! UserData).name
        
        let num:Int = (dataFormAPI[row] as! UserData).id
        
        elementIdLbl.text = " Element ID: \(num)"
    }
    
}
